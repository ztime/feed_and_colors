# feed fetcher thingy and color classification

Oh it's ugly, the codes ugly. But it runs! Well kinda

Try and running a feed first, it will download into folder `solendro`:
```
python extract_csv_and_images_from_feed.py -f --file-format csv --image-link-column image_link --be-nice 'https://feeds.lengow.io/3/64x5p9q' solendro
```

Next to last argument is the feed, and the one after that is the folder to use, try running `python extract_csv_and_images_from_feed.py --help` for more commands
If by some miracle that works, try running the color classification (stolen from here: [https://github.com/ahmetozlu/color_recognition](https://github.com/ahmetozlu/color_recognition)
```
 python find_contours.py -i 'solendro/images/burlington-21984--MorrisCo-face.jpg?v=1641554687' -o solendro --show-inbetween-images
 ```
 This would parse that file, put the rendered complete one in `solendro/predicted_images`! 

 ## What packages do I need?
 No idea, I installed a bunch, but to start with try:
 ```
 pip install pillow opencv-python imutils xmltodict matplotlib scipy webcolors numpy
 ```
 that should get you started!
