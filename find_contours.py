import imutils
import cv2
import argparse
from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np
import webcolors
from color_recognition_api import color_histogram_feature_extraction
from color_recognition_api import knn_classifier
import os
import sys

class ColorLabeler:
    # checking whether the training data is ready
    PATH = './training.data'

    def knn_predict(self, source_image, mask):
        color_histogram_feature_extraction.color_histogram_of_test_image(source_image, mask)
        prediction = knn_classifier.main('training.data', 'test.data')
        print('Detected color is:', prediction)
        return prediction

    def __init__(self):
        # initialize the colors dictionary, containing the color
        # name as the key and the RGB tuple as the value
        colors = OrderedDict({
            "red": (255, 0, 0),
            "green": (0, 255, 0),
            "blue": (0, 0, 255)})
        # allocate memory for the L*a*b* image, then initialize
        # the color names list
        self.lab = np.zeros((len(colors), 1, 3), dtype="uint8")
        self.colorNames = []
        # loop over the colors dictionary
        for (i, (name, rgb)) in enumerate(colors.items()):
            # update the L*a*b* array and the color names list
            self.lab[i] = rgb
            self.colorNames.append(name)
        # convert the L*a*b* array from the RGB color space
        # to L*a*b*
        self.lab = cv2.cvtColor(self.lab, cv2.COLOR_RGB2LAB)
        if os.path.isfile(self.PATH) and os.access(self.PATH, os.R_OK):
            print ('training data is ready, classifier is loading...')
        else:
            print ('training data is being created...')
            open('training.data', 'w')
            color_histogram_feature_extraction.training()
            print ('training data is ready, classifier is loading...')

    def label(self, image, c):
        # construct a mask for the contour, then compute the
        # average L*a*b* value for the masked region
        mask = np.zeros(image.shape[:2], dtype="uint8")
        cv2.drawContours(mask, [c], -1, 255, -1)
        mask = cv2.erode(mask, None, iterations=2)
        self.show_image("mask", mask)
        mean = cv2.mean(image, mask=mask)[:3]
        # initialize the minimum distance found thus far
        minDist = (np.inf, None)
        # loop over the known L*a*b* color values
        print(mean)
        for (i, row) in enumerate(self.lab):
            # compute the distance between the current L*a*b*
            # color value and the mean of the image
            d = dist.euclidean(row[0], mean)
            # if the distance is smaller than the current distance,
            # then update the bookkeeping variable
            if d < minDist[0]:
                minDist = (d, i)
        # return the name of the color with the smallest distance
        return self.colorNames[minDist[1]]

    def label_knn(self, image, c):
        # construct a mask for the contour, then compute the
        # average L*a*b* value for the masked region
        mask = np.zeros(image.shape[:2], dtype="uint8")
        cv2.drawContours(mask, [c], -1, 255, -1)
        mask = cv2.erode(mask, None, iterations=2)
        prediction = self.knn_predict(image, mask)
        return prediction

    def show_image(self, name, image):
        global SHOW_IMAGES
        if not SHOW_IMAGES:
            return
        cv2.namedWindow(name, cv2.WINDOW_KEEPRATIO)
        cv2.imshow(name, image)
        cv2.resizeWindow(name, 1000, 1000)
        cv2.waitKey(0)
        cv2.destroyWindow(name)

SHOW_IMAGES = True # gaaah, globals curse 

def main():
    global SHOW_IMAGES
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--image', help="Image to process")
    parser.add_argument('-f', '--folder', help="Folder to process")
    parser.add_argument('-o', '--output-folder', required=True)
    parser.add_argument('--show-inbetween-images', action='store_true')
    args = parser.parse_args()
    SHOW_IMAGES = args.show_inbetween_images

    images_to_parse = []
    if args.folder is not None:
        images_to_parse.append([ f"{args.folder}/{x}" for x in os.listdir(args.folder)])
    images_to_parse = [item for subl in images_to_parse for item in subl if isinstance(subl, list)]
    if args.image is not None:
        images_to_parse.append(args.image)
    for image_path in images_to_parse:
        image = cv2.imread(image_path)
        resized = imutils.resize(image, width=800)
        ratio = image.shape[0] / float(resized.shape[0])
        show_image("No change", resized)
        gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
        show_image("Gray", gray)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        show_image("Blurred", blurred)
        thresh = cv2.threshold(blurred, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        show_image("Thresh", thresh)
        # countours
        contours_response = cv2.findContours(255-thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # contours_response = cv2.findContours(255-thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours_response)
        # loop over the contours
        color_labeler = ColorLabeler()
        for c in contours:
            # compute the center of the contour
            M = cv2.moments(c)
            try:
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
            except ZeroDivisionError:
                cX = 200
                cY = 200
            # color
            detected_color = color_labeler.label_knn(resized, c)
            # draw the contour and center of the shape on the image
            cv2.drawContours(resized, c, -1, (0, 255, 0), 2)
            cv2.circle(resized, (cX, cY), 7, (255, 255, 255), -1)
            cv2.putText(resized, f"{detected_color}", (cX - 20, cY - 20),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
            # show the image
        show_image("With color contours", resized)
        saved_images_folder = f"{args.output_folder}/predicted_images"
        try:
            os.mkdir(saved_images_folder)
        except FileExistsError as e:
            pass
        save_as = f"{saved_images_folder}/{os.path.basename(image_path)}_predicted"
        cv2.imwrite(save_as, resized)

def show_image(name, image):
    global SHOW_IMAGES
    if not SHOW_IMAGES:
        return
    cv2.namedWindow(name, cv2.WINDOW_KEEPRATIO)
    cv2.imshow(name, image)
    cv2.resizeWindow(name, 1000, 1000)
    cv2.waitKey(0)
    cv2.destroyWindow(name)


if __name__ == '__main__':
    main()
