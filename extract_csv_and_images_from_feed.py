import sys
import pandas as pd
import requests
import os
import argparse
import tempfile
import xmltodict
import pickle
import csv
import time
from collections.abc import MutableMapping
from pprint import pprint


def main():
    parser = argparse.ArgumentParser(
        description="""
    Parses feed and provides a csv with values and all images
    """)
    parser.add_argument('feed')
    parser.add_argument('output_folder')
    parser.add_argument('--skip-images', action='store_true')
    parser.add_argument('--skip-csv-file', action='store_true')
    parser.add_argument('--is-file', action='store_true')
    parser.add_argument('--be-nice', action='store_true')
    parser.add_argument('-f','--force-folder', action='store_true', help="Force output folder, ignore any files in there")
    parser.add_argument('--file-format', choices=['csv','xml'], default=None)
    parser.add_argument('--image-link-column', type=str, default=None)
    parser.add_argument('--download-all-images', action='store_true', help='By default we only download 15 images, set this to download all')
    args = parser.parse_args()
    print(f"Processing feed '{args.feed}' and saving to {args.output_folder}...")
    # Create output folder
    try:
        os.mkdir(args.output_folder) 
    except FileExistsError as e:
        if args.force_folder:
            pass
        else:
            raise(e)
    # Download feed or if its a file, use that
    feed_filename = os.path.basename(args.feed)
    if not args.is_file:
        print(f"Downloading feed to '{args.output_folder}/{feed_filename}'... ", end="")
        feed_filepath = f'{args.output_folder}/{feed_filename}'
        if os.path.exists(feed_filepath):
            print("Already there, skipping download")
        else:
            res = requests.get(args.feed, allow_redirects=True)
            with open(feed_filepath, 'wb') as downloaded_feed:
                downloaded_feed.write(res.content)
            print("Done")
    else:
        feed_filepath = args.feed
    # handle feed, can be xml or csv
    feed_fileformat = args.file_format
    if feed_fileformat is None:
        if feed_filepath.endswith('xml'):
            feed_fileformat = 'xml'
        elif feed_filepath.endswith('csv'):
            feed_fileformat = 'csv'
        else:
            print("Did not know how to handle the filetype, try and force with --file-type")
            exit(1)
    if feed_fileformat == 'xml':
        print('Converting feed from xml to csv...', end="")
        with open(feed_filepath) as xmlfile:
            converted_filename = f"{args.output_folder}/xml_converted.csv"
            if os.path.exists(converted_filename):
                print("Already done, skipping!")
            else:
                xml_as_dict = xmltodict.parse(xmlfile.read())
                # pprint(xml_as_dict)
                all_items = []
                all_keys = set()
                # for item in xml_as_dict['rss']['channel']['item']:
                for item in xml_as_dict['feed']['entry']:
                    flattend_item = flatten_dict(item)
                    all_items.append(flattend_item)
                    all_keys.update(flattend_item.keys())
                with open(f"{args.output_folder}/xml_converted.csv", 'w') as csv_file:
                    writer = csv.DictWriter(csv_file, fieldnames=list(all_keys))
                    writer.writeheader()
                    writer.writerows(all_items)
                print("Done - saved as xml_converted.csv")
                pprint(all_keys)
        feed_filepath = f"{args.output_folder}/xml_converted.csv"
    elif feed_fileformat == 'csv':
        print("Feed is already csv, do nothing")
    # read it 
    try:
        os.mkdir(f"{args.output_folder}/images")
    except FileExistsError as e:
        pass
    # Try to find image column
    if args.skip_images:
        print("Skipping image downloads")
        return
    csv_data = pd.read_csv(feed_filepath)
    possible_column_names = [ args.image_link_column, 'g:image_link', 'image_link' ]
    column_to_use = [ x for x in possible_column_names if x in csv_data.columns ]
    if not column_to_use:
        print("Could not find a suitable column name for images, set one with --image-link-column")
        print("Available columns:")
        for column in csv_data.columns:
            print(f"\t{column}")
    all_images = list(set(csv_data[column_to_use[0]]))
    number_of_images = len(all_images)
    downloaded_images = 0
    download_limit = 15
    failed_downloads = []
    if args.download_all_images:
        download_limit = -1
    for image_url in all_images:
        print(f"Downloaded {downloaded_images}/{number_of_images}...\r", end='')
        filename = f"{args.output_folder}/images/{os.path.basename(str(image_url))}"
        if os.path.isfile(filename):
            downloaded_images += 1
            continue
        try:
            res = requests.get(image_url, allow_redirects=True)
        except requests.exceptions.MissingSchema as e:
            failed_downloads.append((image_url, "Was missing schema, probably nan"))
            continue
        open(filename, 'wb').write(res.content)
        downloaded_images += 1
        if args.be_nice:
            time.sleep(random.random()*2)
        if download_limit > 0 and downloaded_images > download_limit:
            break
    print("\nDone!")
    if failed_downloads:
        print("Some downloads failed:")
        print("\n".join([ f"{x} - {y}" for (x,y) in failed_downloads ]))


def flatten_dict(d: MutableMapping, sep: str= '.') -> MutableMapping:
    [flat_dict] = pd.json_normalize(d, sep=sep).to_dict(orient='records')
    return flat_dict

if __name__ == '__main__':
    main()
